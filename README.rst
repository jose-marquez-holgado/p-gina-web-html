**ASIR (Administración de Sistemas Informáticos en Red)**
======================

:Tarea 3: Creacción de Pag. Web HTML - Lenguaje de Marcas

:Fecha: 14/02/2014

:Autor: José Márquez Holgado


CONCLUSIÓN:
**********
Después de bastantes días leyendo la teroría, tocaba la puesta en marcha. Aunque aparentemente simple, ha dado más de un dolor de cabeza a la hora de trabajar. Esta pagina tiene como objetivo introducirme en el lenguaje de marcas, más concretamente con el código HTML. 

Aunque bastante simple en su funcionamiento y diseńo, me ha servido para avanzar, afianzar conocimientos en la materia y prepararme para el siguiente reto.


WEBGRAFÍA:
**********
* http://www.desarrolloweb.com/

* http://es.html.net/tutorials/html/

* http://es.html.net/

* http://www.w3schools.com/html/default.asp

* https://www.boe.es/boe/dias/2010/02/25/pdfs/BOE-A-2010-3028.pdf

BIBLIOGRAFIA:
*************
Libro de texto "Lenguaje de marcas y sistemas de gestión de información" ed.Garceta